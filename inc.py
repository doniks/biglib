#!/bin/python3

import sys
import re

for line in sys.stdin:
    l = line.strip()
    if re.match(".*biglib_version = ", l):
        s = l.split('"')
        n = s[1]
        print(s[0], '"', int(s[1]) + 1, '"', s[2], sep='')
    else:
        print(l)



