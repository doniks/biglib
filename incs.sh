#!/bin/bash

set -exv

if [ -e incs.tmp ] ; then
    exit 1
fi

for (( i = 0 ; i < 100 ; i++ )) ; do
    ./inc.py < biglib.c > incs.tmp
    mv incs.tmp biglib.c
    git add biglib.c
    git commit -m $i
done
