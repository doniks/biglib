CFLAGS+=-Werror

biglib.a: biglib.o
	ar -rcs $@ $^

biglib.o: biglib.c
	gcc ${CFLAGS} -c $<

test.o: test.c
	gcc ${CFLAGS} -c $<

test: test.o biglib.a 
	gcc -o $@ $^

run:test
	./test

clean:
	rm -rf *.o *.a test
